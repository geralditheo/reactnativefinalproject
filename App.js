import { StatusBar } from 'expo-status-bar';
import React from 'react';

// CyberApp
import CyberApp from './CyberApp';

// test
import OnBoardingScreen from './CyberApp/Screen/OnBoardingScreen';
import LoginScreen from './CyberApp/Screen/LoginScreen';
import RegisterScreen from './CyberApp/Screen/RegisterScreen';
import HomeScreen from './CyberApp/Screen/HomeScreen';

export default function App() {
  return (
    <>
      <CyberApp />
      {/* <OnBoardingScreen /> */}
      {/* <LoginScreen /> */}
      {/* <RegisterScreen /> */}
      {/* <HomeScreen /> */}
      <StatusBar style="auto" hidden={true} animated={true} />
    </>
  );
}

