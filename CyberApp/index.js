import React from 'react'
import { View, Text } from 'react-native'

// Redux
import {store} from './Store'
import {Provider} from 'react-redux'

// Router
import Router from './Router'

export default function index() {
    return (
        <Provider store={store} >
            <Router />
        </Provider>
    )
}
