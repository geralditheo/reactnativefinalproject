import React, { useState } from 'react'
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity, StatusBar } from 'react-native';

// Font
import { useFonts } from 'expo-font';

// Icon
import { Ionicons } from '@expo/vector-icons';

// App Loading
import AppLoading from 'expo-app-loading';

// Axios
import Axios from 'axios';

// Redux
import {useDispatch} from 'react-redux'

import {changeState} from '../Store/features/session'

// Secure Store
import * as SecureStore from 'expo-secure-store';

const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch();

    let [fontLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })

    const [imageSource] = useState(require('./assets/LoginImage.png'))
    const [placeholderUsername] = useState("Please insert your e - mail")
    const [placeholderPassword] = useState("Please insert your password")
    const [seePassword, setSeePassword] = useState(false)

    const [email, setEmail] = useState()
    const [password, setPassword] = useState()

    const axioxErrorHnadling = (error) => {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log("d", error.response.data);
            console.log("s",error.response.status);
            console.log("h", error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          console.log(error.config);
    }

    const loginSubmit = async () => {

        let result = await Axios.post('https://capstone-b21-cap026.herokuapp.com/api/login', {
            email: String(email),
            password: String(password),
        })

        if (result){
            await SecureStore.setItemAsync("session_id", JSON.stringify(
                {
                    token: result.data.token.token
                }
            ))

            let session = await SecureStore.getItemAsync("session_id")
            dispatch(changeState(session))
        }

        console.log("Loged");
    }

    if (!fontLoaded){
        return (
            <AppLoading />
        )
    }else{
        return(
            <View style={styles.bakcground}>
    
                <View style={styles.innerLayout}>
                    <View style={styles.imageLayout} >
                        <Image
                            style={styles.image}
                            source={imageSource}
                        />
                    </View>
                    <View style={styles.titleTextLayout}>
                        <Text style={styles.titleText}>Welcome Back </Text>
                        <Text style={styles.miniTitleText}>Do not forget to log back</Text>
                    </View>
                    <View style={styles.form} >
                        <View style={styles.formPage}>
                            <View style={styles.contentFormUp}>
                                <View style={{marginBottom: 10}}>
                                    <Text style={styles.contentFormUpText}>Username / E - Mail</Text>
                                    <TextInput style={styles.contentInput} placeholder={placeholderUsername} value={email} onChangeText={(value) => setEmail(value)} />
                                </View>
                                <View>
                                    <Text style={styles.contentFormUpText}>Password</Text>
                                    <View style={styles.formInput}>
                                        <TextInput style={styles.formInputField} placeholder={placeholderPassword} secureTextEntry={!seePassword} value={password} onChangeText={(item) => setPassword(item)} />
                                        <TouchableOpacity onPress={() => setSeePassword((status) => !status)} >
                                            {
                                                seePassword ? <Ionicons name="eye" size={24} color="black" /> : <Ionicons name="eye-off" size={24} color="black" />
                                            }
                                            
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                
    
                            </View>
                            <View style={styles.contentFormDown}>
                                <TouchableOpacity onPress={loginSubmit} >
                                    <View style={styles.buttonForm} >
                                        <Text style={styles.buttonFormText} >LOGIN</Text>
                                    </View>
                                </TouchableOpacity>
    
                                <View style={styles.separator} ></View>
    
                                <View style={styles.signIn} >
                                    <Text style={styles.signInSection} >
                                        Dont Have an account ? Please
                                        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')} >
                                            <Text style={styles.signInSectionClick} > sign here </Text>
                                        </TouchableOpacity>
                                        
                                    </Text> 
                                </View>
    
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bakcground:{
        flex:1,
        backgroundColor: '#C2F784'
    },
    innerLayout: {
        flex: 1,
        margin: 20,
        // borderColor: 'black',
        // borderWidth: 2,
    },
    imageLayout: {
        alignItems: 'center',
        padding: 25,
        // borderColor: 'black',
        // borderWidth: 1
    },
    image:{
        height: 150,
        width: 300,
        resizeMode: 'contain',
    },
    titleTextLayout:{
        // borderColor: 'black',
        // borderWidth: 1,
        alignItems: 'center'
    },
    titleText:{
        fontSize: 35,
        color: '#3F3D56',
        fontFamily: 'reem-kufi'
    },
    miniTitleText:{
        fontSize: 18,
        fontFamily: 'reem-kufi',
        color: '#3F3D56'
    },
    form: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1
    },
    formPage:{
        margin: 15,
        padding: 25,
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'space-around',
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 12,
    },
    contentFormUp: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 5,
    },
    contentFormUpText: {
        fontFamily: 'reem-kufi',
        color: '#3F3D56',
        fontSize: 13,
    },
    contentInput:{
        height: 45,
        marginTop: 4,
        padding: 15,
        borderRadius: 15,
        backgroundColor: 'white',
        shadowColor: 'black',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    contentFormDown:{
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 2,
    },
    buttonForm:{
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#C2F784',
        borderRadius: 15,
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    buttonFormText:{
        fontFamily: 'reem-kufi',
        color: '#3F3D56',
        fontSize: 18,
    },
    separator:{
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    signIn:{
        alignItems: 'center'
    },
    signInSection:{
        fontFamily: 'reem-kufi',
        color: '#3F3D56',
        fontSize: 10,
    },
    signInSectionClick:{
        color: '#C2F784'
    },
    formInput:{
        flexDirection: 'row',
        marginTop: 4,
        alignItems: 'center',
        padding: 10,
        borderRadius: 15,
        backgroundColor: 'white',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    formInputField:{
        flex: 1
    }
})

export default LoginScreen;