import React from 'react'
import { StyleSheet, Text, View, Image, Button } from 'react-native'

// OnBoarding
import Onboarding from 'react-native-onboarding-swiper';

// App Loading
import AppLoading from 'expo-app-loading';

// Font
import {useFonts} from 'expo-font';

// Async Storage
import AsyncStorage from '@react-native-async-storage/async-storage';

// Redux
import {useSelector, useDispatch} from 'react-redux'

import {changeFirst} from '../Store/features/firsttimer'

const OnBoardingScreen = () => {

    const dispatch = useDispatch();

    let [fontLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })

    const onDone = async () => {
        try {
            await AsyncStorage.setItem("@storage_Session", String(true) )
            dispatch(changeFirst(true))
            console.log("Done");    
        } catch (error) {
            console.log(error);
        }
        
        
    }

    if (!fontLoaded){
        return (
            <AppLoading />
        )
    }else{
        return (
            <>
                <Onboarding
                    pages={[
                        {
                            backgroundColor: '#FF4848',
                            image: <Image source={require('./assets/Logo.png')} style={styles.imageStyle} />,
                            title: '',
                            subtitle: '',
                        },
                        {
                            backgroundColor: '#FFD371',
                            image: <Image source={require('./assets/onboardingImage1.png')} style={styles.imageStyle} />,
                            title:  <Text style={styles.titleStyle} >Welcome</Text> ,
                            subtitle: <Text style={styles.subtitleStyle} >This App is about a store</Text> ,
                        },
                        {
                            backgroundColor: '#9DDAC6',
                            image: <Image source={require('./assets/onboardingImage2.png')} style={styles.imageStyle} />,
                            title:  <Text style={styles.titleStyle} >Have Fun</Text> ,
                            subtitle: <Text style={styles.subtitleStyle} >We will provide you a better experience</Text> ,
                        },
                    ]}
    
                    showSkip={false}
                    showNext={false}
                    controlStatusBar={true}
                    onDone={onDone}
                    
                />
            </>
        )
    }

}

export default OnBoardingScreen

const styles = StyleSheet.create({
    imageStyle: {
        width: 220,
        height: 180,
        resizeMode: 'center',
    },
    titleStyle:{
        fontSize: 40,
        color: '#3F3D56',
        fontFamily: 'reem-kufi',
    },
    subtitleStyle: {
        fontSize: 15,
        color: '#3F3D56',
        marginTop: 5,
        textAlign: 'center',
        width: 200,
        fontFamily: 'reem-kufi',
    },
})
