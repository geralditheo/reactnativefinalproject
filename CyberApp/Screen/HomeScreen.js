import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity, SafeAreaView, FlatList, Pressable } from 'react-native'

// Font
import {useFonts} from 'expo-font';

// App Loading
import AppLoading from 'expo-app-loading';

// Axios
import Axios from 'axios';

// Secure Store
import * as SecureStore from 'expo-secure-store';

// Redux
import {useDispatch} from 'react-redux'

import {changeState} from '../Store/features/session'

const HomeScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [DATA, setData] = useState()
    const [dataloaded, setDataloaded] = useState(false)

    const getData = async () => {
        await Axios('https://fakestoreapi.com/products')
        .then((response) => {
            setData( response.data )
        }).catch((err) => {
            console.log(err);
        }).finally(() => {
            setDataloaded(true)
        })
    }

    const axioxErrorHnadling = (error) => {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log("d", error.response.data);
            console.log("s",error.response.status);
            console.log("h", error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          console.log(error.config);
    }

    let [profileID, setProfilID]  = useState()
    let [token, setToken] = useState()
    let [type, setType] = useState('bearer') ;
    let [personalData, setPersonalData] = useState()

    const getId = async () => {

        let data = await SecureStore.getItemAsync("session_id")
        data = JSON.parse(data)
        setToken(data.token)

        await Axios.get('https://capstone-b21-cap026.herokuapp.com/api/id', {
            headers: {
                Authorization: type + ' ' + token,
            }
        })
        .then((result) => {
            setProfilID(result.data.id)
        }).catch((error) => {
            axioxErrorHnadling(error)
        });

        return true;
    }

    const getProfile = async () => {

        await Axios.get(`https://capstone-b21-cap026.herokuapp.com/api/profile/${profileID}`,{
            headers: {
                Authorization: type + ' ' + token,
            }
        })
        .then((result) => {
            setPersonalData(result.data.data)
        }).catch((err) => {
            axioxErrorHnadling(err)
        });

        return true;
    }

    const logOutButton = async () => {
        console.log("log out");
        console.log(type);
        console.log(token);

        // const result = await Axios.post('https://capstone-b21-cap026.herokuapp.com/api/logout', {
        //     headers: {
        //         Authorization: type + ' ' + token,
        //     }
        // })

        try {
            await SecureStore.setItemAsync("session_id", String("null"))
            dispatch(changeState(String("null")))
        } catch (error) {
            axioxErrorHnadling(error)
        }

        
        
    }

    useEffect(()=>{
        getData()
        getId()
        getProfile()
        
    },[type, token, profileID, personalData])

    let [fontLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })
    
    if (!fontLoaded || !dataloaded || !personalData ){
        console.log("Fetching Data and Font...");
        return (
            <AppLoading />
        )
    }else {
        return (
            <View>
                <ImageBackground 
                    source={require('./assets/subtractRed.png')}
                    resizeMode={"cover"}
                    style={styles.imageBackgroundUp}
                >
                    <View style={styles.boxWelcome}>
                        <Text style={styles.boxWelcomeText} >Welcome, {personalData.name} </Text>
                    </View>
    
                    <View style={styles.boxTitle}>
                        <View style={styles.boxTitleCover}>
                            <Pressable onPress={logOutButton} style={styles.imageTitle} >
                                
                                    <Image style={styles.imageTitleStyle} source={require('./assets/imageTitle.png')} />
                                
                            </Pressable>
                            <View style={styles.lineRedVertical} >
                                <View style={styles.separator} />
                            </View>
                            <View style={styles.informationTitle} >
                                <View>
                                    <Text style={styles.informationTitleText} >Tuesday</Text>
                                    <Text style={styles.informationTitleText}>28 August 2021</Text>
                                </View>
                                <View>
                                    <Text style={styles.informationTitleText}>32 Degree Celcius</Text>
                                    <Text style={styles.informationTitleText}>Rainy</Text>
    
                                </View>
                                
                            </View>
                        </View>
                    </View>
    
                    <View style={styles.lineRedHorisontal}>
                        <View style={styles.separatorHorizontal}/>
                    </View>

                </ImageBackground>

                <View style={styles.poisonForm}>
                    <View style={styles.poisonTitle} >
                        <Text style={styles.poisonTitleText} >Pick Your Poison</Text>
                    </View>

                    <View style={styles.poisonContainForm} >
                        <SafeAreaView>
                            <FlatList 
                                data={DATA}
                                keyExtractor={(item) => item.id.toString()}
                                renderItem={({item})=>{
                                    // console.log(item);
                                    return(
                                        <>
                                          <View style={styles.itemPoisonContainer} >
                                            <View style={styles.itemPoisonContainerLeft}>
                                                <View style={styles.poisonItemContainer}>

                                                    <Image
                                                        style={{width: 70, height: 70, alignSelf: 'center'}}
                                                        source={{
                                                          uri: `${item.image}`,
                                                        }}
                                                    />
                                                    <View style={styles.poisonItemContainerText} >
                                                        <Text style={styles.poisonItemContainerTextA} > {item.title} </Text>
                                                        <View style={{padding: 2, backgroundColor: '#FF4848', marginRight: 60, marginLeft: 5}} >
                                                            <Text style={[styles.poisonItemContainerTextA, {color: 'white'}]} > {item.category} </Text>
                                                        </View>
                                                        <Text style={styles.poisonItemContainerTextA} > {item.price} </Text>
                                                    </View>
                                                    
                                                </View>
                                                
                                            </View>

                                            <TouchableOpacity onPress={() => {
                                                    console.log("Clicked!")
                                                    console.log(item.id);
                                                    navigation.navigate("DetailProductScreen", {
                                                        id: item.id
                                                    })
                                                }
                                            }>
                                                <View style={styles.itemPoisonContainerRight}>
                                                    <Text style={styles.itemPoisonContainerRightText}>Detail!</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>  
                                        </>
                                    )
                                }}
                            />
                        </SafeAreaView>
                    </View>   
                </View>

                        
            </View>
        )
    }
    


}

export default HomeScreen

const styles = StyleSheet.create({
    imageBackgroundUp: {
        flex:1,
        minHeight:200,
        // borderWidth: 1,
        // borderColor: 'black'
    },
    boxWelcome: {
        // borderColor: 'black',
        // borderWidth: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxWelcomeText: {
        color: '#FFFFFF',
        fontSize: 20,
        fontFamily: 'reem-kufi'
    },
    boxTitle: {
        // borderWidth:1,
        // borderColor: 'black',

        marginVertical: 10,
        marginHorizontal: 40,
        backgroundColor: 'white',
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 12,

    },
    boxTitleCover: {
        // borderWidth:1,
        // borderColor: 'black',

        marginVertical: 20,
        marginHorizontal: 10,
        flexDirection: 'row'
    },
    imageTitle: {
        // borderWidth:1,
        // borderColor: 'black',

        flex: 2,
        padding: 15,
    },
    imageTitleStyle: {
        resizeMode: 'center',
        width: 80,
        height: 80,
    },
    lineRedVertical: {
        // borderWidth:1,
        // borderColor: 'black',

        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    separator:{
        borderRightWidth: 1,
        borderColor: '#FF4848',
        flex: 1,
    },
    informationTitle: {
        // borderWidth:1,
        // borderColor: 'black',

        flex: 3,
        justifyContent: 'space-between',
    },
    informationTitleText: {
        fontSize: 12,
        color: '#3F3D56',
        fontFamily: 'reem-kufi'
    },
    lineRedHorisontal: {
        // borderColor: 'black',
        // borderWidth: 1,
        marginHorizontal: 20,
        marginVertical: 20,
        justifyContent: 'center',
    },
    separatorHorizontal: {
        flex: 1,
        borderColor: '#FF4848',
        borderWidth: 1,
    },
    poisonForm: {
        // borderWidth: 1,
        // borderColor: 'black',

        padding: 15,
        marginTop: 50,
    },
    poisonTitle: {
        // borderWidth: 1,
        // borderColor: 'black',

        justifyContent: 'center',
        alignItems: 'center',
    },
    poisonTitleText: {
        fontFamily: 'reem-kufi',
        fontSize: 20,
    },
    poisonContainForm: {
        // borderWidth: 1,
        // borderColor: 'black',
        height: 250,

        marginTop: 15,
        paddingHorizontal: 5,
        paddingVertical: 1,
        backgroundColor: 'white',
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    itemPoisonContainer: {
        // borderWidth: 1,
        // borderColor: 'black',
        marginVertical: 10,

        height: 100,
        
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    itemPoisonContainerLeft: {
        // borderWidth: 1,
        // borderColor: 'black',

        flex:1,
        padding: 8,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
    },
    poisonItemContainer: {
        flexDirection: 'row',
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
    },
    poisonItemContainerText:{
        // borderColor: 'black', 
        // borderWidth: 1, 

        flex:1,
        paddingLeft: 10,
        justifyContent: 'space-around'
    },
    poisonItemContainerTextA: {
        color: 'black',
        fontFamily: 'reem-kufi',
        fontSize: 10,
    },
    itemPoisonContainerRight: {
        // borderWidth: 1,
        // borderColor: 'black',
        
        flex: 1,
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15,
        backgroundColor: '#FF4848',
    },
    itemPoisonContainerRightText: {
        fontSize: 15,
        color: '#3F3D56',
        transform: [
            {rotate: "90deg"},
            {translateX: 20},
            {translateY: 0},
        ]
    },
})
