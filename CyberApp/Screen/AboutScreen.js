import React from 'react'
import { View, Text, StyleSheet, ImageBackground, Image, Pressable } from 'react-native';

// font
import { useFonts } from 'expo-font'

// App Loading
import AppLoading from 'expo-app-loading';

const AboutScreen = () => {

    let [fontLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })

    if (!fontLoaded){
        return (
            <AppLoading />
        )
    }else {
        return(
            <View>
                <ImageBackground
                    source={require('./assets/subtractYellow.png')}
                    resizeMode='cover'
                    style={styles.imageBackgroundUp}
                >
                    <View style={{flexDirection: 'row'}} >
                        
                        <View style={styles.outliners} >
                            
                        </View>
                        

                        <View style={styles.outlinersTitle}>
                            <Text style={styles.outlinersTitleText} >About</Text>
                        </View>
                        <View style={styles.outliners}>
                            
                        </View>
                    </View>
                    
                    <View style={styles.formAboutBox}>

                        <View style={styles.picProfile}>
                            <Image 
                                source={require('./assets/photo.png')} 
                                style={{width: 100, height: 100}}
                            
                            />

                            <View style={styles.separator} />

                            <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
                                <Text style={[styles.defaultText, {fontSize: 20, textAlign: 'center' }]} >Geraldi Theo Wira Kusuma</Text>
                            </View>
                            
                        </View>

                        <View style={styles.socialMediaContent} >

                            <Text style={[styles.defaultText, {fontSize: 18}]}>Social Media</Text>

                            <View style={styles.socialMediaContentBox}>
                                <View style={styles.socialMediaContentBoxItem} >
                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/twitter.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                        <Text style={styles.defaultText}>geralditheo</Text>
                                    </View>

                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/insta.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                        <Text style={styles.defaultText}>geralditheo</Text>
                                    </View>
                                </View>

                                <View style={styles.socialMediaContentBoxItem} >
                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/face.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                            <Text style={styles.defaultText}>geralditheo</Text>
                                        </View>

                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/tele.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                        <Text style={styles.defaultText}>geralditheo</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.socialMediaContent} >

                            <Text style={[styles.defaultText, {fontSize: 18}]}>Other Links</Text>

                            <View style={styles.socialMediaContentBox}>
                                <View style={styles.socialMediaContentBoxItem} >
                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/gitlab.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                        <Text style={styles.defaultText}>geralditheo</Text>
                                    </View>

                                </View>

                                <View style={styles.socialMediaContentBoxItem} >

                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                        <Image 
                                            source={require('./assets/github.png')} 
                                            style={{height: 30,width: 30, marginRight: 7}} 
                                        />
                                        <Text style={styles.defaultText}>geralditheo</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageBackgroundUp:{
        flex: 1,
        minHeight: 200,
    },
    outliners:{
        flex:1,
        // borderWidth: 2,
        // borderColor: 'black',
        padding: 15
    },
    outlinersTitle:{
        flex:1,
        // borderWidth: 2,
        // borderColor: 'black',
        padding: 15,
        alignItems: 'center'
    },
    outlinersTitleText:{
        color: 'white',
        fontSize: 20,
        fontFamily: 'reem-kufi'
    },
    formAboutBox:{
        // borderWidth: 2,
        // borderColor: 'black',
        borderRadius: 15,
        marginHorizontal: 30,
        marginTop: 30,
        height: 480,
        backgroundColor: 'white',

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 12,
    },
    picProfile:{
        flexDirection: 'row',
        // borderWidth: 2,
        // borderColor: 'black',
        padding: 15
    },
    separator:{
        marginHorizontal: 10,
        borderRightColor: '#FFD371',
        borderRightWidth: StyleSheet.hairlineWidth,
    },
    socialMediaContent:{
        padding: 15,
    },
    socialMediaContentBox:{
        backgroundColor: 'white',
        borderRadius: 15,
        // borderWidth: 2,
        // borderColor: 'black',
        padding: 8,
        marginTop: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 12,
    },
    socialMediaContentBoxItem:{
        flexDirection: 'row',
        // borderWidth: 2,
        // borderColor: 'black',
        marginBottom: 15
    },
    defaultText:{
        color: '#3F3D56',
        fontFamily: 'reem-kufi',
    },

})

export default AboutScreen;