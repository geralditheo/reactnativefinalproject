import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, ImageBackground, Pressable, Image, TouchableOpacity } from 'react-native'

// Icon
import { Ionicons } from '@expo/vector-icons';

// Font
import {useFonts} from 'expo-font';

// App Loading
import AppLoading from 'expo-app-loading';

// Axios
import Axios from 'axios';

const DetailProductScreen = ({route, navigation}) => {

    const {id} = route.params;

    const [DATA, setData] = useState()
    const [dataloaded, setDataloaded] = useState(false)

    const getDatum = async (id) => {
        await Axios.get(`https://fakestoreapi.com/products/${id}`)
        .then((response) => {
            setData( response.data )
        }).catch((err) => {
            console.log(err);
        }).finally(() => {
            setDataloaded(true)
        })
    }

    let [fontLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })

    useEffect(()=>{
        getDatum(id)
    },[])

    if (!fontLoaded || !dataloaded){
        return (
            <AppLoading />
        )
    }else {

        return (
            <View>
                <ImageBackground 
                    source={require('./assets/subtractRed.png')}
                    resizeMode={"cover"}
                    style={styles.imageBackgroundUp}
                >
    
                    <View style={styles.upperMenu} >
                        <Pressable onPress={() => { navigation.navigate("HomeScreen") }} style={[styles.upperMenuItem, styles.upperMenuItemLeft]} >
                            <Ionicons name="arrow-back" size={30} color="white" />
                        </Pressable>
    
                        <View style={[styles.upperMenuItem, styles.upperMenuItemCenter]} >
                            <Text style={{fontSize: 20, color: 'white' }} >Detail</Text>
                        </View>
    
                        <Pressable onPress={() => console.log("Right")} style={[styles.upperMenuItem,styles.upperMenuItemRight]} >
                            <Ionicons name="basket" size={30} color="white" />
                        </Pressable>
                    </View>
    
                    <View style={styles.buttonTitle} >
                        <View style={styles.buttonTitleContainer}>
                            <View style={styles.buttonTitleLeft} >
                                <Text>TItle  {DATA.title} </Text>
                            </View>
                            <TouchableOpacity style={styles.buttonTitleRIght} onPress={() => console.log("ADD")}>
                                <View  >
                                    <Text style={styles.buttonTitleRIghtText} >ADD</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
    
                </ImageBackground>
    
                
                <View style={styles.detailForm} >
                    <View style={styles.detailFormBox} >
                        {console.log(DATA)}
                        <View style={styles.detailImage}>
                            <Image
                                style={{alignSelf: 'center', resizeMode: 'center', width: 150, height: 150 }}
                                source={{
                                    uri: `${DATA.image}`,
                                }}
                            />
                        </View>
                        <View style={styles.detailPrice}>
                            <Text>Price: {DATA.price}</Text>
                        </View>
                        <View style={styles.detailCategory}>
                            <Text>Category: {DATA.category} </Text>
                        </View>
                        <View>
                            <Text>Count: {DATA.rating.count} </Text>
                        </View>
                        <View>
                            <Text>Rate: {DATA.rating.rate} </Text>
                        </View>
                        <View style={styles.detailDescription}>
                            <Text>Desc: {DATA.description} </Text>
                        </View>
                    </View>
                </View>
                
                
            </View>
        )
    }

}

export default DetailProductScreen

const styles = StyleSheet.create({
    imageBackgroundUp: {
        flex:1,
        minHeight:200,
        // borderWidth: 1,
        // borderColor: 'black'
    },
    upperMenu: {
        flexDirection: 'row',
    },
    upperMenuItem: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1,
        paddingVertical: 10,
        justifyContent: 'center',
    },
    upperMenuItemLeft: {
        alignItems: 'flex-start', 
        paddingLeft: 10,
    },
    upperMenuItemCenter: {
        alignItems: 'center', 
        flex: 2,
    },
    upperMenuItemRight: {
        alignItems: 'flex-end', 
        paddingRight: 10,
    },
    buttonTitle: {
        // borderWidth: 1,
        // borderColor: 'black',
        marginVertical: 15,
        paddingVertical: 15,
        paddingHorizontal: 15,
    },
    buttonTitleContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        height: 50,
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 10,
    },
    buttonTitleLeft: {
        flex:1,
        // borderWidth: 1,
        // borderColor: 'black',
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        justifyContent: 'center',
        paddingLeft: 15,
    },
    buttonTitleRIght: {
        // borderWidth: 1,
        // borderColor: 'black',
        paddingHorizontal: 15,
        backgroundColor: 'red',
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15,
        alignItems: 'center',
        justifyContent:'center'
    },
    buttonTitleRIghtText: {
        color: '#573E55'
    },
    detailForm: {
        // borderWidth: 1, 
        // borderColor:'black', 
        padding: 5, 
        height: 500,
    },
    detailFormBox: {
        marginHorizontal: 10, 
        marginVertical: 5, 
        backgroundColor: 'white', 
        flex: 1,
        borderRadius: 15,
        padding: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 10,
    },
    detailImage: {
        // borderWidth: 1,
        // borderColor: 'black',

        flex:4
    },
    detailPrice: {
        // borderWidth: 1,
        // borderColor: 'black',

    },
    detailCategory: {
        // borderWidth: 1,
        // borderColor: 'black',

    },
    detailDescription: {
        // borderWidth: 1,
        // borderColor: 'black',

        flex:4
    }

})
