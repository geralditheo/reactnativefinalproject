import React, { useState } from 'react'
import { StyleSheet, Text, View, StatusBar, TextInput, TouchableOpacity } from 'react-native'

// Loader
import AppLoading from 'expo-app-loading'

// Font
import { useFonts } from 'expo-font'

// Icon
import { Ionicons } from '@expo/vector-icons';

// Axios
import Axios from 'axios';

const RegisterScreen = ({navigation}) => {
    const [fonstLoaded] = useFonts({
        'reem-kufi': require('./fonts/reem-kufi.ttf')
    })

    const [seePassword, setSeePassword] = useState(false)
    const [seeRePassword, setSeeRePassword] = useState(false)

    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [repassword, setRepassword] = useState()

    const axioxErrorHnadling = (error) => {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log("d", error.response.data);
            console.log("s",error.response.status);
            console.log("h", error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          console.log(error.config);
    }

    const registerSubmit = async () => {
        let result = await Axios.post('https://capstone-b21-cap026.herokuapp.com/api/register',{
            name: name,
            email: email,
            password: password,
            repassword: repassword
        })

        if (result){
            console.warn("Regis Success");
            navigation.navigate("LoginScreen");
        }
        

    }

    if(fonstLoaded){
        return (
            <View style={styles.backgroundContainer} >
                <StatusBar 
                    animated={true}
                    backgroundColor= 'white'
                    hidden={true}
                />

                <View style={styles.titleContainer} >
                    <View>
                        <Text style={styles.upperTitle} >Welcome</Text>
                    </View>
                    <View style={styles.downerTitleContainer}>
                        <Text style={styles.downerTitle} >Before we go step further please fill the form first to get to know each other</Text>
                    </View>
                </View>
                <View style={styles.formContainer} >
                    <View style={styles.formBox}>
                        <View style={styles.formFill} >
                            <View style={styles.formFillName} >
                                <Text style={styles.formTextDefault} >Name</Text>
                                <View style={styles.formInput}>
                                    <TextInput placeholder="Please insert your name" style={styles.formTextInput} value={name} onChangeText={(value) => setName(value) } />
                                </View>
                            </View>
                            <View style={styles.formFillName} >
                                <Text style={styles.formTextDefault} >E - Mail</Text>
                                <View style={styles.formInput}>
                                    <TextInput placeholder="Please insert your email" style={styles.formTextInput} value={email} onChangeText={(value) => setEmail(value)}  />
                                </View>
                            </View>
                            <View style={styles.formFillName} >
                                <Text style={styles.formTextDefault} >Password</Text>
                                <View style={styles.formInput}>
                                    <TextInput 
                                        placeholder="Please insert your password" 
                                        style={styles.formTextInput} 
                                        secureTextEntry={!seePassword}
                                        value={password} 
                                        onChangeText={(value) => setPassword(value)}
                                    />

                                    <TouchableOpacity onPress={ () => setSeePassword( (status) => !status ) } >
                                        {
                                            seePassword ? <Ionicons name="eye" size={24} color="black" /> :  <Ionicons name="eye-off" size={24} color="black" />
                                        }
                                    </TouchableOpacity>
                                        
                                </View>
                            </View>
                            <View style={styles.formFillName} >
                                <Text style={styles.formTextDefault} >Re - Password</Text>
                                <View style={styles.formInput}>
                                    <TextInput 
                                        placeholder="Please insert your re password" 
                                        style={styles.formTextInput}
                                        secureTextEntry={!seeRePassword}
                                        value={repassword} 
                                        onChangeText={(value) => setRepassword(value)}
                                    />
                                    <TouchableOpacity onPress={ () => setSeeRePassword( (status) => !status ) }> 
                                        {
                                            seeRePassword ? <Ionicons name="eye" size={24} color="black" /> :  <Ionicons name="eye-off" size={24} color="black" />
                                        }
                                    </TouchableOpacity>


                                    
                                </View>
                            </View>
                        </View>
                        <View style={styles.formSubmit}>
                            <View >
                                <TouchableOpacity onPress={registerSubmit}>
                                    <View style={styles.formSubmitSignUp}>
                                        <Text style={styles.formTextDefault} >SIGN UP</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.formSubmitLogin}>
                                <View style={styles.separator} ></View>
                                <Text style={styles.logInSection} >
                                    Already have an account ? you can
                                    <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')} >
                                        <Text style={styles.logInSectionClick} > log here </Text>
                                    </TouchableOpacity>
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }else{
        return <AppLoading />
    }
}

export default RegisterScreen

const styles = StyleSheet.create({
    backgroundContainer:{
        flex: 1,
        backgroundColor: '#C2F784'
    },
    titleContainer:{
        flex:1,
        // borderWidth: 1,
        // borderColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    upperTitle:{
        fontFamily: 'reem-kufi',
        fontSize: 40,
        color: '#3F3D56',
    },
    downerTitleContainer:{
        // borderWidth: 1,
        // borderColor: 'black',
        paddingHorizontal: 60,
    },
    downerTitle:{
        fontFamily: 'reem-kufi',
        textAlign: 'center',
        color: '#3F3D56',
        fontSize: 15,
    },
    formContainer:{
        flex:3,
        // borderWidth: 1,
        // borderColor: 'black',
        padding: 20,
    },
    formBox:{
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
        backgroundColor: 'white',
        borderRadius: 15,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 12,
    },
    formFill:{
        flex:2,
        // borderWidth: 1,
        // borderColor: 'black',
        paddingTop: 15,
        paddingHorizontal: 30,
    },
    formSubmit:{
        flex:1,
        // borderWidth: 1,
        // borderColor: 'black',
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent: 'flex-end',
    },
    formFillName:{
        marginBottom: 10,
    },
    formInput:{
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        marginTop: 5,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,
        elevation: 6,

        // borderWidth: 1,
        // borderColor: 'black',
    },
    formTextInput:{
        flex: 1,
    },
    formTextDefault:{
        fontFamily: 'reem-kufi',
        color: '#3F3D56',
        fontSize: 12,
    },
    formSubmitSignUp:{
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#C2F784',
        borderRadius: 15,
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.50,
        shadowRadius: 10.00,

        elevation: 6,
    },
    formSubmitLogin:{
        // borderWidth: 2,
        // borderColor: 'black',

        marginTop: 15,
        justifyContent: 'center',
        marginHorizontal: 25,
    },
    separator:{
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    logInSection:{
        alignSelf: 'center',
        fontFamily: 'reem-kufi',
        color: '#3F3D56',
        fontSize: 10,
    },
    logInSectionClick:{
        color: '#C2F784'
    },
})
