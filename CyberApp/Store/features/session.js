import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    value: "null"
}

export const sessionSlice = createSlice({
    name: 'session',
    initialState: initialState,
    reducers: {
        changeState: (state, action) =>{
            state.value = action.payload
        }
    }
})

export const {changeState} = sessionSlice.actions;

export default sessionSlice.reducer;