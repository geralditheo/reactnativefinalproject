import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    value: "true",
}

export const firsttimerSlice = createSlice({
    name: 'firsttimer',
    initialState: initialState,
    reducers: {
        changeFirst: (state, action) =>{
            state.value = action.payload
        }
    }
})

export const {changeFirst} = firsttimerSlice.actions;

export default firsttimerSlice.reducer;