import {configureStore} from '@reduxjs/toolkit';
import sessionReduce from './features/session';
import firsttimerReduce from './features/firsttimer';

export const store = configureStore({
    reducer: {
        session: sessionReduce,
        firsttimer: firsttimerReduce,
    }
})