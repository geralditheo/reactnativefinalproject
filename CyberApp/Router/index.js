import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native'

// Navigation Container
import {NavigationContainer} from '@react-navigation/native';

// Stack
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Tab
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Icon
import { Ionicons } from '@expo/vector-icons';

// Secure Store
import * as SecureStore from 'expo-secure-store';

// Redux
import {useSelector, useDispatch} from 'react-redux'

import {changeState} from '../Store/features/session'
import { changeFirst } from '../Store/features/firsttimer';

// Async Storage
import AsyncStorage from '@react-native-async-storage/async-storage';

// Component
import OnBoardingScreen from '../Screen/OnBoardingScreen';
import LoginScreen from '../Screen/LoginScreen';
import RegisterScreen from '../Screen/RegisterScreen';
import HomeScreen from '../Screen/HomeScreen';
import AboutScreen from '../Screen/AboutScreen';
import DetailProductScreen from '../Screen/DetailProductScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();


export default function index() {

    const dispatch = useDispatch();

    // session
    const selector = useSelector((state) => {
        return state.session.value;
    })

    // first
    const firstTimer = useSelector((state) => {
        return state.firsttimer.value;
    })


    const getSession = async () => {
        await SecureStore.getItemAsync("session_id")
        .then((result) => {
            dispatch(changeState(String(result)))
        })
    }

    const getFirst = async () => {
        const value = await AsyncStorage.getItem("@storage_Session")
        .then((result)=>{
            dispatch(changeFirst(result))
        })
        
    }
    
    useEffect(() => {
        getFirst()
        getSession()

    }, [])



    console.log(firstTimer);

    if (!firstTimer){
        return (
            <>
                <NavigationContainer>
                    <Stack.Navigator screenOptions={{headerShown:false}} >
                        <Stack.Screen name='OnBoardingScreen' component={OnBoardingScreen}  />
                    </Stack.Navigator>
                </NavigationContainer>
            </>
        )
    }else if (selector == "null"){
        return (
            <NavigationContainer>
                <Stack.Navigator screenOptions={{headerShown:false}} >
                    <Stack.Screen name={"LoginScreen"} component={LoginScreen} options={{gestureDirection: 'horizontal'}} />
                    <Stack.Screen name={"RegisterScreen"} component={RegisterScreen} options={{gestureDirection: 'horizontal'}} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }else {
        return (
            <>
                <NavigationContainer>
                    <Stack.Navigator screenOptions={{headerShown:false}} >
                        <Stack.Screen name={"HomeScreen"} component={HomeTabSCreen} />
                        <Stack.Screen name={"DetailProductScreen"} component={DetailProductScreen} />
                    </Stack.Navigator>
                </NavigationContainer>
            </>
        )
    }
        
        

}


const HomeTabSCreen = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                iconName = focused
                    ? 'home'
                    : 'home-outline';
                } else if (route.name === 'About') {
                iconName = focused ? 'aperture' : 'aperture-outline';
                }

                return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'gray',
            })}
        >
            <Tab.Screen name={"Home"} component={HomeScreen} options={{headerShown: false}} />
            <Tab.Screen name={"About"} component={AboutScreen} options={{headerShown: false}} />
        </Tab.Navigator>
    )
}